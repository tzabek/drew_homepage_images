<?php

if( ! class_exists( 'DREW_Shortcode' ) ) {
	/**
	 * Handles plugin shortcode
	 */
	class DREW_Shortcode extends DREW_Module {
		protected static $readable_properties  = array();
		protected static $writeable_properties = array();
		const SHORTCODE_NAME = 'drew_homepage_image';

		/*
		 * Magic methods
		 */
		/**
		 * Constructor
		 *
		 * @mvc Controller
		 */
		protected function __construct() {
			$this->register_hook_callbacks();
		}

		/*
		 * Static methods
		 */

		/**
		 * Defines the [drew_homepage_image] shortcode
		 *
		 * @mvc Controller
		 *
		 * @param array $attributes
		 * return string
		 */
		public static function create_shortcode( $attributes ) {
			$attributes = apply_filters( 'drew_shortcode-attributes', $attributes );
			$attributes = self::validate_drew_shortcode_attributes( $attributes );

			/* Retrieve images */
			$homepage_images = DREW_Homepage_Images::get_images();
			$homepage_images = $homepage_images['basic'];
			$inline_css_ary = array();
			
			/* Display image depending on current time */
			$image_src = self::get_image_src($homepage_images);
			
			$background = (bool)$attributes['background'];
			
			/* Build inline CSS for background image */
			if( $background ):
				$inline_css_ary[] = 'background-image:url('.$image_src.');';
				/* Fullwidth background? */
				if($attributes['bg-size'] == 'cover'):
					$inline_css_ary[] = '-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover';
				else:
					$inline_css_ary[] = 'background-size:'.$attributes['bg-size'];
				endif;
				/* Attachment */
				$inline_css_ary[] = 'background-attachment:'.$attributes['bg-attachment'];
				/* Position */
				$inline_css_ary[] = 'background-position:'.$attributes['bg-position'];
			
				/* Create inline css */
				$inline_css = implode(';',$inline_css_ary);
				$inline_css = ' style="'.$inline_css.'"';
			else:
				$image = '<img id="HomepageImage" class="HomepageImage" src="'.$image_src.'">';
				$image_tag = $image;
				$wrap = (bool)$attributes['wrap'];
				if( $wrap ):
					$wrap_tag = $attributes['wrap-tag'];
					$wrap_id = $attributes['wrap-id'];
					$wrap_class = $attributes['wrap-class'];
					$wrap_style = $attributes['wrap-style'];
			
					$image_tag = '<'.$wrap_tag.' id="'.$wrap_id.'" class="'.$wrap_class.'" style="'.$wrap_style.'">'.$image.'</'.$wrap_tag.'>';
				endif;
			endif;
			
			$drew_homepage_image = $background ? $inline_css : $image_tag;

			return self::render_template( 'drew-shortcode/drew-shortcode.php', array( 'drew_homepage_image' => $drew_homepage_image ) );
		}

		/**
		 * Retrieves correct image based on current time
		 *
		 * @mvc Controller
		 *
		 * @param array $homepage_images
		 * return string
		 */
		public static function get_image_src( $homepage_images ) {
			$current_time = current_time("G");

			if( $current_time >= 6 && $current_time < 13 ):
				$image_src = $homepage_images['field-homepage_image_1'];
			elseif( $current_time >= 13 && $current_time < 18 ):
				$image_src = $homepage_images['field-homepage_image_2'];
			elseif( $current_time >= 18 ):
				$image_src = $homepage_images['field-homepage_image_3'];
			endif;

			return $image_src;
		}

		/**
		 * Validates the attributes for the [drew_homepage_image] shortcode
		 *
		 * @mvc Model
		 *
		 * @param array $attributes
		 * return array
		 */
		protected static function validate_drew_shortcode_attributes( $attributes ) {
			$defaults   = self::get_default_drew_shortcode_attributes();
			$attributes = shortcode_atts( $defaults, $attributes );
			return apply_filters( 'drew_validate-drew-shortcode-attributes', $attributes );
		}
		/**
		 * Defines the default arguments for the [drew_homepage_image] shortcode
		 *
		 * @mvc Model
		 *
		 * @return array
		 */
		protected static function get_default_drew_shortcode_attributes() {
			$attributes = array(
				'background' => 0, // Is it background image?
				'bg-size' => 'cover', // Background Size
				'bg-attachment' => 'fixed', // Background Attachment
				'bg-position' => 'center center', // Background Position
				'wrap' => 0, // Are we wrapping the image?
				'wrap-tag' => 'div', // Tag
				'wrap-id' => '', // ID
				'wrap-class' => '', // Classes
				'wrap-style' => '' // Additional style (inline)
			);
			return apply_filters( 'drew_default-drew-shortcode-attributes', $attributes );
		}

		/**
		 * Register callbacks for actions and filters
		 *
		 * @mvc Controller
		 */
		public function register_hook_callbacks() {
			add_action( 'init', array( $this, 'init' ) );
			add_shortcode( self::SHORTCODE_NAME, __CLASS__ . '::create_shortcode' );
		}
		/**
		 * Prepares site to use the plugin during activation
		 *
		 * @mvc Controller
		 *
		 * @param bool $network_wide
		 */
		public function activate( $network_wide ) {}
		/**
		 * Rolls back activation procedures when de-activating the plugin
		 *
		 * @mvc Controller
		 */
		public function deactivate() {
		}
		/**
		 * Initializes variables
		 *
		 * @mvc Controller
		 */
		public function init() {
		}
		/**
		 * Executes the logic of upgrading from specific older versions of the plugin to the current version
		 *
		 * @mvc Model
		 *
		 * @param string $db_version
		 */
		public function upgrade( $db_version = 0 ) {
		}
		/**
		 * Checks that the object is in a correct state
		 *
		 * @mvc Model
		 *
		 * @param string $property An individual property to check, or 'all' to check all of them
		 *
		 * @return bool
		 */
		protected function is_valid( $property = 'all' ) {
			return true;
		}
	}
}

?>