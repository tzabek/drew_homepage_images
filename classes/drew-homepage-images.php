<?php

if ( ! class_exists( 'DREW_Homepage_Images' ) ) {

	/**
	 * Handles plugin homepage_images
	 */
	class DREW_Homepage_Images extends DREW_Module {
		protected $homepage_images;
		protected static $default_homepage_images;
		protected static $readable_properties  = array( 'homepage_images' );
		protected static $writeable_properties = array( 'homepage_images' );

		const REQUIRED_CAPABILITY = 'administrator';


		/*
		 * General methods
		 */

		/**
		 * Constructor
		 *
		 * @mvc Controller
		 */
		protected function __construct() {
			$this->register_hook_callbacks();
		}

		/**
		 * Public setter for protected variables
		 *
		 * Updates homepage_images outside of the Homepage Images API or other subsystems
		 *
		 * @mvc Controller
		 *
		 * @param string $variable
		 * @param array  $value This will be merged with DREW_Homepage_Images->homepage_images, so it should mimic the structure of the DREW_Homepage_Images::$default_homepage_images. It only needs the contain the values that will change, though. See DREW_Skeleton->upgrade() for an example.
		 */
		public function __set( $variable, $value ) {
			// Note: DREW_Module::__set() is automatically called before this

			if ( $variable != 'homepage_images' ) { return; }

			$this->homepage_images = self::validate_homepage_images( $value );
			update_option( 'drew_homepage_images', $this->homepage_images );
		}

		/**
		 * Register callbacks for actions and filters
		 *
		 * @mvc Controller
		 */
		public function register_hook_callbacks() {
			add_action( 'admin_menu',            __CLASS__ . '::register_homepage_images_pages' );

			add_action( 'init',                  array( $this, 'init' ) );
			add_action( 'admin_init',            array( $this, 'register_homepage_images' ) );

			add_filter( 'plugin_action_links_' . plugin_basename( dirname( __DIR__ ) ) . '/drew.php', __CLASS__ . '::add_plugin_action_links' );
		}

		/**
		 * Prepares site to use the plugin during activation
		 *
		 * @mvc Controller
		 *
		 * @param bool $network_wide
		 */
		public function activate( $network_wide ) {
		}

		/**
		 * Rolls back activation procedures when de-activating the plugin
		 *
		 * @mvc Controller
		 */
		public function deactivate() {
		}

		/**
		 * Initializes variables
		 *
		 * @mvc Controller
		 */
		public function init() {
			self::$default_homepage_images = self::get_default_homepage_images();
			$this->homepage_images         = self::get_homepage_images();
		}

		/**
		 * Executes the logic of upgrading from specific older versions of the plugin to the current version
		 *
		 * @mvc Model
		 *
		 * @param string $db_version
		 */
		public function upgrade( $db_version = 0 ) {
			/*
			if( version_compare( $db_version, 'x.y.z', '<' ) )
			{
				// Do stuff
			}
			*/
		}

		/**
		 * Checks that the object is in a correct state
		 *
		 * @mvc Model
		 *
		 * @param string $property An individual property to check, or 'all' to check all of them
		 * @return bool
		 */
		protected function is_valid( $property = 'all' ) {
			// Note: __set() calls validate_homepage_images(), so homepage_images are never invalid

			return true;
		}


		/*
		 * Plugin Homepage Images
		 */

		/**
		 * Establishes initial values for all homepage_images
		 *
		 * @mvc Model
		 *
		 * @return array
		 */
		protected static function get_default_homepage_images() {
			$basic = array(
				'field-homepage_image_1' => '',
				'field-homepage_image_2' => '',
				'field-homepage_image_3' => ''
			);

			/* ::TODO::
			 * 
			 * Add 'Advanced' => 'Settings' to defaults
			 */

			return array(
				'db-version' => '0',
				'basic'      => $basic
			);
		}

		/**
		 * Retrieves all of the homepage_images from the database
		 *
		 * @mvc Model
		 *
		 * @return array
		 */
		protected static function get_homepage_images() {
			$homepage_images = shortcode_atts(
				self::$default_homepage_images,
				get_option( 'drew_homepage_images', array() )
			);

			return $homepage_images;
		}

		/**
		 * Retrieves all of the homepage_images from the database
		 * Used to retrieve images for DREW_Shortcode::create_shortcode()
		 *
		 * @mvc Model
		 *
		 * @return array
		 */
		public static function get_images() {
			return self::get_homepage_images();
		}

		/**
		 * Adds links to the plugin's action link section on the Plugins page
		 *
		 * @mvc Model
		 *
		 * @param array $links The links currently mapped to the plugin
		 * @return array
		 */
		public static function add_plugin_action_links( $links ) {
			array_unshift( $links, '<a href="options-general.php?page=' . 'drew_homepage_images">Images Upload</a>' );

			return $links;
		}

		/**
		 * Adds pages to the Admin Panel menu
		 *
		 * @mvc Controller
		 */
		public static function register_homepage_images_pages() {
			add_submenu_page(
				'options-general.php',
				DREW_NAME . ' Homepage Images',
				DREW_NAME,
				self::REQUIRED_CAPABILITY,
				'drew_homepage_images',
				__CLASS__ . '::markup_homepage_images_page'
			);
		}

		/**
		 * Creates the markup for the Homepage Images page
		 *
		 * @mvc Controller
		 */
		public static function markup_homepage_images_page() {
			if ( current_user_can( self::REQUIRED_CAPABILITY ) ) {
				echo self::render_template( 'drew-homepage-images/page-homepage-images.php' );
			} else {
				wp_die( 'Access denied.' );
			}
		}

		/**
		 * Registers homepage_images sections, fields and homepage_images
		 *
		 * @mvc Controller
		 */
		public function register_homepage_images() {
			/*
			 * Basic Section
			 */
			add_settings_section(
				'drew_section-basic',
				'',
				__CLASS__ . '::markup_section_headers',
				'drew_homepage_images'
			);

			add_settings_field(
				'drew_field-homepage_image_1',
				'Morning Image<br/><span class="description">This image will appear in the morning (6am - 1pm)</span>',
				array( $this, 'markup_fields' ),
				'drew_homepage_images',
				'drew_section-basic',
				array( 'label_for' => 'drew_field-homepage_image_1' )
			);
			add_settings_field(
				'drew_field-homepage_image_2',
				'Afternoon Image<br/><span class="description">This image will appear in the afternoon (1pm - 6pm)</span>',
				array( $this, 'markup_fields' ),
				'drew_homepage_images',
				'drew_section-basic',
				array( 'label_for' => 'drew_field-homepage_image_2' )
			);
			add_settings_field(
				'drew_field-homepage_image_3',
				'Evening Image<br/><span class="description">This image will appear at night (6pm - 6am)</span>',
				array( $this, 'markup_fields' ),
				'drew_homepage_images',
				'drew_section-basic',
				array( 'label_for' => 'drew_field-homepage_image_3' )
			);

			// The homepage_images container
			register_setting(
				'drew_homepage_images',
				'drew_homepage_images',
				array( $this, 'validate_homepage_images' )
			);
		}

		/**
		 * Adds the section introduction text to the Homepage Images page
		 *
		 * @mvc Controller
		 *
		 * @param array $section
		 */
		public static function markup_section_headers( $section ) {
			echo self::render_template( 'drew-homepage-images/page-homepage-images-section-headers.php', array( 'section' => $section ), 'always' );
		}


		/**
		 * Delivers the markup for homepage_images fields
		 *
		 * @mvc Controller
		 *
		 * @param array $field
		 */
		public function markup_fields( $field ) {
			echo self::render_template( 'drew-homepage-images/page-homepage-images-fields.php', array( 'homepage_images' => $this->homepage_images, 'field' => $field ), 'always' );
		}

		/**
		 * Validates submitted setting values before they get saved to the database. Invalid data will be overwritten with defaults.
		 *
		 * @mvc Model
		 *
		 * @param array $new_homepage_images
		 * @return array
		 */
		public function validate_homepage_images( $new_homepage_images ) {
			$new_homepage_images = shortcode_atts( $this->homepage_images, $new_homepage_images );

			if ( ! is_string( $new_homepage_images['db-version'] ) ) {
				$new_homepage_images['db-version'] = DREW_Skeleton::VERSION;
			}

			return $new_homepage_images;
		}
	} // end DREW_Homepage_Images
}
