<div class="uploader_wrap">
	<form method="post" action="options.php">
		<h1>
			<span class="img inline-block"><img src="<?php echo plugins_url('/images/drew-logo1.png',dirname(dirname( __FILE__ ))); ?>"></span>
			<span class="text inline-block"><?php esc_html_e( DREW_NAME ); ?></span>
			<div class="submit"><?php submit_button( 'Save Images', 'drew-submit', 'drew-save-images' ); ?></div>
		</h1>
		<?php settings_fields( 'drew_homepage_images' ); ?>
		<?php do_settings_sections( 'drew_homepage_images' ); ?>
		<h2 class="shortcode">
			<span>How to display images using a shortcode</span>
			<div class="the-shortcode">
				<span class="shortcode-wrapper">[drew_homepage_image]</span>
			</div>
		</h2>
		<section id="ShortcodeOptions">
			<table class="form-table" id="ShortcodeTable">
				<thead>
					<tr>
						<th>Parameter</th>
						<th>Description</th>
						<th>Required</th>
						<th>Values</th>
						<th>Default</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><strong>background</strong>
						</td>
						<td>Is it a background image?</td>
						<td>optional</td>
						<td>1,0</td>
						<td>0</td>
					</tr>
					<tr>
						<td><strong>bg-size</strong>
						</td>
						<td>Ignored if parameter 'background' set to 0. Defines background size.</td>
						<td>optional</td>
						<td>auto,length,cover,contain,initial,inherit</td>
						<td>cover</td>
					</tr>
					<tr>
						<td><strong>bg-attachment</strong>
						</td>
						<td>Igonered if parameter 'background' set to 0. Defines background attachment.</td>
						<td>optional</td>
						<td>scroll,fixed,local,initial,inherit</td>
						<td>fixed</td>
					</tr>
					<tr>
						<td><strong>bg-position</strong>
						</td>
						<td>Ignored if parameter 'background' set to 0. Defines background position.</td>
						<td>optional</td>
						<td><a href="http://www.w3schools.com/cssref/pr_background-position.asp" rel="nofollow">http://www.w3schools.com/cssref/pr_background-position.asp</a>
						</td>
						<td>center center</td>
					</tr>
					<tr>
						<td><strong>wrap</strong>
						</td>
						<td>Ignored if parameter 'background' set to 1. Wrap image with HTML tag</td>
						<td>optional</td>
						<td>1,0</td>
						<td>0</td>
					</tr>
					<tr>
						<td><strong>wrap-tag</strong>
						</td>
						<td>Ignored if parameter 'wrap' set to 0. Defines HTML tag the image will be wrapped with</td>
						<td>optional</td>
						<td>any HTML tag</td>
						<td>div</td>
					</tr>
					<tr>
						<td><strong>wrap-id</strong>
						</td>
						<td>Ignored if parameter 'wrap' set to 0. Sets the ID of image wrapper.</td>
						<td>optional</td>
						<td>custom wrapper ID</td>
						<td>''</td>
					</tr>
					<tr>
						<td><strong>wrap-class</strong>
						</td>
						<td>Ignored if parameter 'wrap' set to 0. Sets the class(es) of image wrapper.</td>
						<td>optional</td>
						<td>custom wrapper classes</td>
						<td>''</td>
					</tr>
					<tr>
						<td><strong>wrap-style</strong>
						</td>
						<td>Ignored if parameter 'wrap' set to 0. Additional inline styles.</td>
						<td>optional</td>
						<td>custom wrapper styles</td>
						<td>''</td>
					</tr>
				</tbody>
			</table>
		</section>
	</form>
</div> <!-- .wrap -->
