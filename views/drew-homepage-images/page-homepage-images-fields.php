<?php
/*
 * Fields
 */
?>

<?php if ( 'drew_field-homepage_image_1' == $field['label_for'] ) : ?>

	<section id="upload_image_1">
		<div class="image_preview" id="preview_1" style="background-image:url('<?php esc_attr_e( $homepage_images['basic']['field-homepage_image_1'] ); ?>');">
		</div>
		<div class="hide">
			<input type="hidden" name="<?php esc_attr_e( 'drew_homepage_images[basic][field-homepage_image_1]' ); ?>" id="<?php esc_attr_e( 'drew_homepage_images[basic][field-homepage_image_1]' ); ?>" value="<?php esc_attr_e( $homepage_images['basic']['field-homepage_image_1'] ); ?>">
		</div>
		<div class="upload-image-wrap">
			<input type="button" value="Upload Image" class="button-secondary button-small" data-id="upload_image" id="1"/>
		</div>
	</section>

<?php elseif ( 'drew_field-homepage_image_2' == $field['label_for'] ) : ?>

	<section id="upload_image_2">
		<div class="image_preview" id="preview_2" style="background-image:url('<?php esc_attr_e( $homepage_images['basic']['field-homepage_image_2'] ); ?>');"></div>
		<div class="hide">
			<input type="hidden" name="<?php esc_attr_e( 'drew_homepage_images[basic][field-homepage_image_2]' ); ?>" id="<?php esc_attr_e( 'drew_homepage_images[basic][field-homepage_image_2]' ); ?>" value="<?php esc_attr_e( $homepage_images['basic']['field-homepage_image_2'] ); ?>">
		</div>
		<div class="upload-image-wrap">
			<input type="button" value="Upload Image" class="button-secondary button-small" data-id="upload_image" id="2"/>
		</div>
	</section>

<?php elseif ( 'drew_field-homepage_image_3' == $field['label_for'] ) : ?>

	<section id="upload_image_3">
		<div class="image_preview" id="preview_3" style="background-image:url('<?php esc_attr_e( $homepage_images['basic']['field-homepage_image_3'] ); ?>');"></div>
		<div class="hide">
			<input type="hidden" name="<?php esc_attr_e( 'drew_homepage_images[basic][field-homepage_image_3]' ); ?>" id="<?php esc_attr_e( 'drew_homepage_images[basic][field-homepage_image_3]' ); ?>" value="<?php esc_attr_e( $homepage_images['basic']['field-homepage_image_3'] ); ?>">
		</div>
		<div class="upload-image-wrap">
			<input type="button" value="Upload Image" class="button-secondary button-small" data-id="upload_image" id="3"/>
		</div>
	</section>

<?php endif; ?>
