<?php
/*
Plugin Name: DREW Homepage Images
Plugin URI:  
Description: Object-oriented/MVC WordPress plugin allowing user to upload up to 3 pictures displayed on the homepage that will alternate automatically dependant on the time of day
Version:     0.1a
Author:      Tom Zabek
*/

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Access denied.' );
}

define( 'DREW_NAME',                 'Homepage Images' );
define( 'DREW_REQUIRED_PHP_VERSION', '5.3' ); // because of get_called_class()
define( 'DREW_REQUIRED_WP_VERSION',  '3.1' ); // because of esc_textarea()

/**
 * Checks if the system requirements are met
 *
 * @return bool True if system requirements are met, false if not
 */
function drew_requirements_met() {
	global $wp_version;
	//require_once( ABSPATH . '/wp-admin/includes/plugin.php' );		// to get is_plugin_active() early

	if ( version_compare( PHP_VERSION, DREW_REQUIRED_PHP_VERSION, '<' ) ) {
		return false;
	}

	if ( version_compare( $wp_version, DREW_REQUIRED_WP_VERSION, '<' ) ) {
		return false;
	}

	/*
	if ( ! is_plugin_active( 'plugin-directory/plugin-file.php' ) ) {
		return false;
	}
	*/

	return true;
}

/**
 * Prints an error that the system requirements weren't met.
 */
function drew_requirements_error() {
	global $wp_version;

	require_once( dirname( __FILE__ ) . '/views/requirements-error.php' );
}

/*
 * Check requirements and load main class
 * The main program needs to be in a separate file that only gets loaded if the plugin requirements are met.
 * Otherwise older PHP installations could crash when trying to parse it.
 */
if ( drew_requirements_met() ) {
	require_once( __DIR__ . '/classes/drew-module.php' );
	require_once( __DIR__ . '/classes/drew-skeleton.php' );
	require_once( __DIR__ . '/includes/admin-notice-helper/admin-notice-helper.php' );
	require_once( __DIR__ . '/classes/drew-shortcode.php' );
	require_once( __DIR__ . '/classes/drew-homepage-images.php' );
	require_once( __DIR__ . '/classes/drew-instance-class.php' );

	if ( class_exists( 'DREW_Skeleton' ) ) {
		$GLOBALS['drew'] = DREW_Skeleton::get_instance();
		register_activation_hook(   __FILE__, array( $GLOBALS['drew'], 'activate' ) );
		register_deactivation_hook( __FILE__, array( $GLOBALS['drew'], 'deactivate' ) );
	}
} else {
	add_action( 'admin_notices', 'drew_requirements_error' );
}
?>