/**
 * Wrapper function to safely use $
 */
function drewFrontWrapper( $ ) {
	var drew = {

		/**
		 * Main entry point
		 */
		init: function () {
			drew.prefix      = 'drew_';
			drew.registerEventHandlers();
		},

		/**
		 * Registers event handlers
		 */
		registerEventHandlers: function () {}
	}; // end drew

	$( document ).ready( drew.init );

} // end drewFrontWrapper()

drewFrontWrapper( jQuery );
