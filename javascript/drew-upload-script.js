/**
 * Admin function to safely use $
 */
function drewAdminWrapper( $ ) {
	var drew = {

		/**
		 * Main entry point
		 */
		init: function () {
			drew.prefix      = 'drew_admin';
			// Register event handlers
			drew.registerEventHandlers();
		},

		/**
		 * Registers event handlers
		 */
		registerEventHandlers: function () {
			/* Picture Upload */
			$(document).on( 'click', '[data-id="upload_image"]', drew.uploadHomepageImage );
			/* Save */
			$(document).on( 'submit', '.uploader_wrap form', drew.saveHomepageImages );
		},

		/**
		 * Upload Image
		 *
		 * @param object event
		 */
		uploadHomepageImage: function ( ev ) {
			var homepage_image_id = $(this).attr('id');
			var image = wp.media({ 
				title: 'Upload Image',
				multiple: false
			})
			.open()
			.on('select', function(e){
				// This will return the selected image from the Media Uploader, the result is an object
				var uploaded_image = image.state().get('selection').first();
				// We convert uploaded_image to a JSON object to make accessing it easier
				// Output to the console uploaded_image
				var image_url = uploaded_image.toJSON().url;
				// Let's assign the url value to the input field
				$('section#upload_image_'+homepage_image_id+' input[type="hidden"]').val(image_url);
				// Display image preview
				$('#preview_'+homepage_image_id).attr('style','background-image:url('+image_url+');');
			});
			ev.preventDefault();
		},

		/**
		 * Save Homepage Images
		 *
		 * @param object event
		 */
		saveHomepageImages: function ( form ) {
			var image_upload_form = $(this);
			image_upload_form.find('h1 > div.submit .ButtonAnimation').remove();
			image_upload_form.find('h1 > div.submit').append('<div class="ButtonAnimation"></div>');
		}
	}; // end drew

	$( document ).ready( drew.init );

} // end drewAdminWrapper()

drewAdminWrapper( jQuery );