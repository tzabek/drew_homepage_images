## DREW Homepage Images
Object-oriented/MVC WordPress plugin allowing user to upload 3 pictures displayed on the homepage that will alternate automatically dependant on the time of day.

![Screenshot 2016-03-19 13.12.42.png](https://bitbucket.org/repo/y4eXAB/images/1738566058-Screenshot%202016-03-19%2013.12.42.png)

### INSTALLATION
1. Download plugin
2. UNZIP the content to your WordPress Plugin directory.
3. Log in to WordPress Admin and go to Plugins page. Activate "DREW Homepage Images".

### PLUGIN USAGE
To upload images go to Settings -> Homepage Images. Please note that required capability to upload images is set to "administrator"


#### SHORTCODE OPTIONS:

    [drew_homepage_image]

Parameter  | Description | Required | Values | Default
------------- | ------------- | ------------- | ------------- | -------------
**background**  | Is it a background image? | optional | 1,0 | 0
**bg-size** | Ignored if parameter 'background' set to 0. Defines background size. | optional | auto,length,cover,contain,initial,inherit | cover
**bg-attachment** | Igonered if parameter 'background' set to 0. Defines background attachment. | optional | scroll,fixed,local,initial,inherit | fixed
**bg-position** | Ignored if parameter 'background' set to 0. Defines background position. | optional | http://www.w3schools.com/cssref/pr_background-position.asp | center center
**wrap** | Ignored if parameter 'background' set to 1. Wrap image with HTML tag | optional | 1,0 | 0
**wrap-tag** | Ignored if parameter 'wrap' set to 0. Defines HTML tag the image will be wrapped with | optional | any HTML tag | div
**wrap-id** | Ignored if parameter 'wrap' set to 0. Sets the ID of image wrapper. | optional | custom wrapper ID | ''
**wrap-class** | Ignored if parameter 'wrap' set to 0. Sets the class(es) of image wrapper. | optional | custom wrapper classes | ''
**wrap-style** | Ignored if parameter 'wrap' set to 0. Additional inline styles. | optional | custom wrapper styles | ''

#### SHORTCODE USAGE

##### Use default

    [drew_homepage_image] or <?php echo do_shortcode('[drew_homepage_image]'); ?>
--------------
##### Set background image
To set uploaded images as dynamic background image, you will need to use WordPress function "do_shortcode()" inside page BODY tag, for example:

    Try this code:

    <body <?php echo do_shortcode('[drew_homepage_image background="1"]'); ?>>

    You can specify your own background image attributes, for example:

    <body <?php echo do_shortcode('[drew_homepage_image background="1" bg-size="cover" bg-attachment="scroll" bg-position="right top"]'); ?>>
---------------
##### Wrap image with any HTML tag
To wrap your image with SECTION tag try this code:

    [drew_homepage_image background="0" wrap="1" wrap-tag="section"]

    You can specify your own wrapper attributes, for example:

    [drew_homepage_image background="0" wrap="1" wrap-tag="section" wrap-id="my-image-wrapper" wrap-class="img-container img-wrapper" wrap-class="display:inline-block"]

### TODO:

* Include default images instead of empty slots
* Write new module (**DREW_Metaboxes**) to include functionality inside homepage edit screen
* Rewrite **DREW_Instance_Class** (replace dummy protected vars with actual ones)
* Update **DREW_Shortcode** module **MVC** structure; make sure the shortcode is generated inside **Controller** rather than **View**